<?php
include 'lib.php';
header::Ses_session();
Incref::Ref();
class homepage {
	public static function call_home_page() {
		Tem_Header::Tem_Bheader();
		Tem_Header::Tem_Load_pages(array('nav-left','sl-control-caption'));
		Tem_Header::Tem_Load_pages_single(array('list-6','list','list-7','list-8','list-9','list-10','bar'));
		Tem_Header::Tem_Load_pages(array('lists','foolter-a'));

		Tem_Header::Tem_Bfoolter();
	}
	public static function call_login_page(){
		Tem_Header::Tem_Bheader();
		Tem_Header::Tem_Load_pages(array('nav-icon','../form/login-2','foolter-a'));
		Tem_Header::Tem_Bfoolter();
	}

	public static function call_product(){
		Tem_Header::Tem_Bheader();
		Tem_Header::Tem_Load_pages(array('nav-left'));
		Tem_Header::Tem_Load_pages_single(array('list-6'));
		Tem_Header::Tem_Load_pages(array('foolter-a'));
		Tem_Header::Tem_Bfoolter();
	}

	public static function call_contact(){
		Tem_Header::Tem_Bheader();
		Tem_Header::Tem_Load_pages(array('nav-left'));
		Tem_Header::Tem_Load_pages_single(array('contact'));
		Tem_Header::Tem_Load_pages(array('foolter-a'));
		Tem_Header::Tem_Bfoolter();
	}
	public static function call_table(){
		Tem_Header::Tem_Bheader();
		Tem_Header::Tem_Load_pages(array('nav-left'));
		Tem_Header::Tem_Load_pages_single(array('table'));
		Tem_Header::Tem_Load_pages(array('foolter-a'));
		Tem_Header::Tem_Bfoolter();
	}
	public static function call_login_admin(){
		Tem_Header::Tem_Bheader();
		Tem_Header::Tem_Load_pages(array('nav-icon','../form/login-3','foolter-a'));
		Tem_Header::Tem_Bfoolter();
	}
}
?>

