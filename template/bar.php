      <div class="container-fluid" style="background-color:#f5f5f5;">
      <div class="container pt-lg-3">
        <h2 class="h3 mb-3 pb-4  text-center">Why our marketplace?</h2>
        <div class="row pt-lg-2 text-center">
          <div class="col-lg-3 col-sm-6 mb-grid-gutter">
            <div class="d-inline-flex align-items-center text-start"><img src="https://cartzilla.createx.studio/img/marketplace/features/quality.png" width="52" alt="Quality Guarantee">
              <div class="ps-3">
                <h6 class=" fs-base mb-1">Quality Guarantee</h6>
                <p class=" fs-ms opacity-70 mb-0">Quality checked by our team</p>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-sm-6 mb-grid-gutter">
            <div class="d-inline-flex align-items-center text-start"><img src="https://cartzilla.createx.studio/img/marketplace/features/support.png" width="52" alt="Customer Support">
              <div class="ps-3">
                <h6 class=" fs-base mb-1">Customer Support</h6>
                <p class=" fs-ms opacity-70 mb-0">Friendly 24/7 customer support</p>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-sm-6 mb-grid-gutter">
            <div class="d-inline-flex align-items-center text-start"><img src="https://cartzilla.createx.studio/img/marketplace/features/updates.png" width="52" alt="Free Updates">
              <div class="ps-3">
                <h6 class=" fs-base mb-1">Lifetime Free Updates</h6>
                <p class=" fs-ms opacity-70 mb-0">Never pay for an update</p>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-sm-6 mb-grid-gutter">
            <div class="d-inline-flex align-items-center text-start"><img src="https://cartzilla.createx.studio/img/marketplace/features/secure.png" width="52" alt="Secure Payments">
              <div class="ps-3">
                <h6 class=" fs-base mb-1">Secure Payments</h6>
                <p class=" fs-ms opacity-70 mb-0">We posess SSL / Secure сertificate</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br>
      </div>