<section class="container pb-4 pt-lg-5 pb-sm-5 pb-4">
      <h2 class="text-center pt-5 pt-sm-2">Trending food in your city</h2>
      <div class="row pt-4 mt-2 mt-lg-3 mb-md-2">
        <div class="col-lg-6 mb-grid-gutter">
          <div class="d-block d-sm-flex justify-content-between align-items-center bg-faded-info rounded-3">
            <div class="pt-5 py-sm-5 px-4 ps-md-5 pe-md-0 text-center text-sm-start">
              <h2>Become a Courier</h2>
              <p class="text-muted pb-2">Earn competitive salary as delivery courier working flexible schedule.</p><a class="btn btn-primary" href="#">Start earning</a>
            </div><img class="d-block mx-auto mx-sm-0" src="https://cartzilla.createx.studio/img/food-delivery/courier.png" width="272" alt="Become a Courier">
          </div>
        </div>
        <div class="col-lg-6 mb-grid-gutter">
          <div class="d-block d-sm-flex justify-content-between align-items-center bg-faded-warning rounded-3">
            <div class="pt-5 py-sm-5 px-4 ps-md-5 pe-md-0 text-center text-sm-start">
              <h2>Become a Partner</h2>
              <p class="text-muted pb-2">Grow your business by reaching new customers.</p><a class="btn btn-primary" href="#">Partner with us</a>
            </div><img class="d-block mx-auto mx-sm-0" src="https://cartzilla.createx.studio/img/food-delivery/chef.png" width="269" alt="Become a Partner">
          </div>
        </div>
      </div>
    </section>